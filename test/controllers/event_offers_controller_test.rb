require 'test_helper'

class EventOffersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get event_offers_new_url
    assert_response :success
  end

  test "should get show" do
    get event_offers_show_url
    assert_response :success
  end

end
