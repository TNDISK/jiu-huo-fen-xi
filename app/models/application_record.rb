class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  # 複数で使うので共通化
  scope :not_sent, -> { where(admin_checked: false)}
end
