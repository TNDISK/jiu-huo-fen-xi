class Event < ApplicationRecord
  belongs_to :admin_user
  has_many :event_offers,  dependent: :destroy
  validates :date, presence: true
end
