class QuestionsController < ApplicationController
  before_action :authenticate_admin_user!, only: %i[show update]

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(params_question)
    if @question.save
      flash[:notice] = "お問い合わせありがとうございます。\n管理者からメールが届きますのでお待ちください。"
      redirect_to root_path
    else
      render 'new'
    end
  end

  # ここは管理者のみ
  def show
    @question = Question.find(params[:id])
  end

  # ここは管理者のみ
  def update
    Question.find(params[:id]).update_attribute(:admin_checked, true)
    flash[:notice] = "対応済みにしたヨ"
    redirect_to current_admin_user
  end


  private
  def params_question
    params.require(:question).permit(:name, :email, :title, :content)
  end
end
