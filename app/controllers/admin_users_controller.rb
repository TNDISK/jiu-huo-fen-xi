class AdminUsersController < ApplicationController
  before_action :authenticate_admin_user!
  def show
    # Todo:あとで絞り込み実装(全て、未対応(対応済み))
    @event_offers     = EventOffer.not_sent
    @all_event_offers = EventOffer.all

    @questions        = Question.not_sent
    @all_questions    = Question.all
  end
end
