class EventsController < ApplicationController
  before_action :authenticate_admin_user!, only: %i[new create destroy]
  before_action :set_event, only: %i[show destroy]

  def index
    @events = Event.all.order(date: "DESC")
  end

  def show
    @event_offer = @event.event_offers.build
  end

  def new
    @event = current_admin_user.events.build
  end

  def create
    @event = current_admin_user.events.create(params_events)
    flash[:notice] = "イベントを作成したヨ"
    redirect_to events_path
  end

  def destroy
    @event.destroy
    flash[:notice] = "イベント削除したヨ"
    redirect_to events_path
  end

  private
    def set_event
      @event = Event.find(params[:id])
    end

    def params_events
      params.require(:event).permit(:title, :content, :place, :date)
    end

end
