class PostsController < ApplicationController
  before_action :authenticate_admin_user!, only: %i[new create destroy edit update]
  before_action :set_post, only: %i[show destroy edit update]

  def index
    @posts = Post.all.order(id: "DESC")
  end

  def show; end

   # ここから管理者のみ↓ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
  def new
    @post = current_admin_user.posts.build
  end

  def create
    @post = current_admin_user.posts.create(params_posts)
    redirect_to @post
  end

  def edit; end

  def update
    if @post.update(params_posts)
      flash[:notice] = "日記を編集したヨ"
      redirect_to posts_path
    else
      render edit
    end
  end

  def destroy
    @post.destroy
    flash[:notice] = "日記削除したヨ"
    redirect_to posts_path
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

    def params_posts
      params.require(:post).permit(:title, :content)
    end
end
