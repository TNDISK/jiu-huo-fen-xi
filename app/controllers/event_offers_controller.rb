class EventOffersController < ApplicationController
  before_action :set_event
  before_action :authenticate_admin_user!, only: %i[show update]

  def new
    @event_offer = @event.event_offers.build
  end

  def create
    @event_offer = @event.event_offers.build(params_event_offer)
    if @event_offer.save
      flash[:notice] = "ご応募ありがとうございます。\n管理者からメールが届きますのでお待ちください。"
      redirect_to events_path
    else
      render 'event_offers/new'
    end
  end

  # ここは管理者のみ
  def show
    @event_offer = EventOffer.find(params[:id])
  end

  # ここは管理者のみ
  def update
    EventOffer.find(params[:id]).update_attribute(:admin_checked, true)
    flash[:notice] = "送信済みにしたヨ"
    redirect_to admin_user_path
  end

  private
    def set_event
      @event = Event.find(params[:event_id])
    end

    def params_event_offer
      params.require(:event_offer).permit(:name, :email, :remark, :event_id)
    end


end
