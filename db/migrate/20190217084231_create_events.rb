class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :place
      t.text :content
      t.references :admin_user, foreign_key: true

      t.timestamps
    end
  end
end
