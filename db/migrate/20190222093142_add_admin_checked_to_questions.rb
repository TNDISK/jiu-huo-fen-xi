class AddAdminCheckedToQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :admin_checked, :boolean, default: false, null: false
  end
end
