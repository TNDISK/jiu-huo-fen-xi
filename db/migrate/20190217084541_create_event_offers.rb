class CreateEventOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :event_offers do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.text :remark
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
