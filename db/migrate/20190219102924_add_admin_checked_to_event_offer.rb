class AddAdminCheckedToEventOffer < ActiveRecord::Migration[5.2]
  def change
    add_column :event_offers, :admin_checked, :boolean, default: false, null: false
  end
end
