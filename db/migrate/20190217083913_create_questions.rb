class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
