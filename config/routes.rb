Rails.application.routes.draw do
  devise_for :admin_users, skip: :all
  devise_scope :admin_user do
    get 'admin/login' => 'devise/sessions#new', as: :new_admin_user_session
    post 'login' => 'devise/sessions#create', as: :admin_user_session
    delete 'logout' => 'devise/sessions#destroy', as: :destroy_admin_user_session
  end
  resources :events, only: %i[index show new create destroy] do
    resources :event_offers, only: %i[new create show update]
  end

  resources :posts
  resources :questions, only: %i[show new create update]

  resource :admin_user, only: %i[show]



  root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
